#include <iostream>
#include <thread>
#include <vector>
#include <algorithm>

using namespace std;

template<class T>
class ListNode{
  public:
    T data;
    ListNode<T> *next;
    ListNode<T>(int _data){
      data = _data;
    }
};

template <class T>
class List{
  private:
    int numberOfNodes;
    ListNode<T> *head;

  public:
    List(){
      numberOfNodes = 0;
    }

    void insertNode(T data){
      //insert the head
      ListNode<T> *temp = new ListNode<T>(data);
      temp->next = head;
      head = temp;
      numberOfNodes++;
    }

    int count() const {
      return numberOfNodes;
    }
};

void theFun(List<int> &list){
  for(int i=0; i<100; i++){
    list.insertNode(i);
  }
}

int main(){
  List<int> list;
  vector<thread> threads;
  for(int i=0; i<10; i++){
    threads.push_back(thread(&theFun, ref(list)));
  }
  
  for_each(threads.begin(), threads.end(), [](thread &th){
    th.join();
  });

  cout << list.count() << endl;
  return 0;
}
