GCC=g++
FLAGS= -Wall -std=c++0x -g 
OUTFILE1= ThreadEx1
OUTFILE2= ThreadEx2
OUTFILE3= ThreadEx3
OUTFILE4= ThreadEx4
INFILE1= thread.cpp
INFILE2= thread1.cpp
INFILE3= thread2.cpp
INFILE4= thread3.cpp
LIBRARY= -lpthread -static-libgcc -static-libstdc++

all: $(OUTFILE1) $(OUTFILE2) $(OUTFILE3) $(OUTFILE4)
	
$(OUTFILE1): $(INFILE1)
	$(GCC) -o $(OUTFILE1) $(INFILE1) $(FLAGS) $(LIBRARY)

$(OUTFILE2): $(INFILE2)
	$(GCC) -o $(OUTFILE2) $(INFILE2) $(FLAGS) $(LIBRARY)

$(OUTFILE3): $(INFILE3)
	$(GCC) -o $(OUTFILE3) $(INFILE3) $(FLAGS) $(LIBRARY)

$(OUTFILE4): $(INFILE4)
	$(GCC) -o $(OUTFILE4) $(INFILE4) $(FLAGS) $(LIBRARY)
