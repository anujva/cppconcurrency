#include <iostream>
#include <string>
#include <thread>
#include <future>

using namespace std;

void theFun(promise<string> &prms){
  prms.set_value("Hello from Thread");
}

int main(){
  promise<string> prms;
  future<string> ftr = prms.get_future();
  thread th(&theFun, ref(prms));
  string str = ftr.get();
  cout << str << endl;
  cout << "Hello from main" << endl;
  th.join();
}
