#include <iostream>
#include <thread>
#include <vector>

using namespace std;

int main(){
  vector<thread> threads;
  for(int i=0; i < 10; i++){
    threads.push_back(thread([i](){
      cout << "Hello from thread " << i << "\n";
    }));
  }
  cout << "Hello World\n";
  for(int i = 0; i < (int)threads.size(); i++){
    threads[i].join();
  }
  return 0;
}
